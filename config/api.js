// 以下是业务服务器API地址
// 本机开发时使用
//var WxApiRoot = 'http://localhost:8083/api/';
// 局域网测试使用
var WxApiRoot = 'http://51wangshi.com:8083/api/';
// 云平台部署时使用
// var WxApiRoot = 'http://122.51.199.160:8080/wx/';
// 云平台上线时使用
// var WxApiRoot = 'https://www.menethil.com.cn/wx/';

module.exports = {
 diyDetail:WxApiRoot +'single/sms/diyDetail',
 homeNewProduct:WxApiRoot +'single/sms/homeNewProduct/list', //首页新品推荐列表
 homeBrand:WxApiRoot +'single/sms/homeBrand/list', //首页品牌推荐列表
 homeRecommendProduct:WxApiRoot +'single/sms/homeRecommendProduct/list', //首页人气推荐列表
 homeRecommendSubject:WxApiRoot +'single/sms/homeRecommendSubject/list', //首页专题推荐列表

  IndexUrl: WxApiRoot + 'home/index', //首页数据接口
  AboutUrl: WxApiRoot + 'home/about', //介绍信息
  bannerList: WxApiRoot +'single/home/bannerList', // 首页banner
  CatalogList: WxApiRoot + 'single/pms/catalog/index', //分类目录全部分类数据接口
  CatalogCurrent: WxApiRoot + 'single/pms/catalog/current', //分类目录当前分类数据接口

  AuthLoginByWeixin: WxApiRoot + 'applet/login_by_weixin2', //微信登录
  AuthLoginByAccount: WxApiRoot + 'single/home/login', //账号登录
  AuthLogout: WxApiRoot + 'auth/logout', //账号登出
  AuthRegister: WxApiRoot + 'single/home/reg', //账号注册
  AuthReset: WxApiRoot + 'single/user/resetPassword', //账号密码重置
  AuthRegisterCaptcha: WxApiRoot + 'auth/regCaptcha', //验证码
  AuthBindPhone: WxApiRoot + 'auth/bindPhone', //绑定微信手机号

  GoodsCount: WxApiRoot + 'single/pms/goodsCount', //统计商品总数
  GoodsList: WxApiRoot + 'single/pms/goods/list', //获得商品列表
  GoodsCategory: WxApiRoot + 'single/pms/goods/category', //获得分类数据
  GoodsDetail: WxApiRoot + 'single/pms/goods/detail1', //获得商品的详情
  GoodsRelated: WxApiRoot + 'goods/related', //商品详情页的关联商品（大家都在看）
typeGoodsList:  WxApiRoot +'single/pms/categoryAndGoodsList/list', //查询商品类型下的商品列表
categoryList: WxApiRoot + 'single/pms/productCategory/list', // 查询商品分类列表
productAttrCategoryList: WxApiRoot +'single/pms/productAttrCategory/list',
  BrandList: WxApiRoot + 'single/pms/brand/list', //品牌列表
  BrandDetail: WxApiRoot + 'single/pms/brand/detail', //品牌详情

  CartList: WxApiRoot + 'cart/list', //获取购物车的数据
  CartAdd: WxApiRoot + 'cart/addCart', // 添加商品到购物车
  CartFastAdd: WxApiRoot + 'cart/fastadd', // 立即购买商品
  CartUpdate: WxApiRoot + 'cart/update/quantity', // 更新购物车的商品
  CartDelete: WxApiRoot + 'cart/delete', // 删除购物车的商品
  CartChecked: WxApiRoot + 'cart/checked', // 选择或取消选择商品
  CartGoodsCount: WxApiRoot + 'cart/getnumber', // 获取购物车商品件数
  CartCheckout: WxApiRoot + 'single/oms/submitPreview', // 下单前信息确认

  CollectList: WxApiRoot + 'collection/listCollect', //收藏列表
  CollectAddOrDelete: WxApiRoot + 'collection/favoriteSave', //添加或取消收藏

  CommentList: WxApiRoot + 'single/pms/consult/list', //评论列表
  CommentCount: WxApiRoot + 'comment/count', //评论总数
  CommentPost: WxApiRoot + 'single/cms/addSubjectCom', //发表评论

  TopicList: WxApiRoot + 'single/cms/subject/list', //专题列表
  TopicDetail: WxApiRoot + 'single/cms/subject/detail', //专题详情
  TopicCommentList: WxApiRoot + 'single/cms/subjectComment/list', //相关专题

  SearchIndex: WxApiRoot + 'single/search/index', //搜索关键字
  SearchResult: WxApiRoot + 'single/search/result', //搜索结果
  SearchHelper: WxApiRoot + 'single/search/helper', //搜索帮助
  SearchClearHistory: WxApiRoot + 'single/search/clearhistory', //搜索历史清楚

  AddressList: WxApiRoot + 'address/list', //收货地址列表
  AddressDetail: WxApiRoot + 'address/detail', //收货地址详情
  AddressSave: WxApiRoot + 'address/save', //保存收货地址
  AddressDelete: WxApiRoot + 'address/delete', //保存收货地址

  ExpressQuery: WxApiRoot + 'express/query', //物流查询

  RegionList: WxApiRoot + 'region/list', //获取区域列表

  OrderSubmit: WxApiRoot + 'single/oms/generateStoreOrder', // 提交订单
  //OrderSubmit: WxApiRoot + 'single/oms/generateOrder', // 提交订单
  OrderPrepay: WxApiRoot + 'pay/weixinAppletPay', // 订单的预支付会话
  OrderList: WxApiRoot + 'single/oms/order/list', //订单列表
  OrderDetail: WxApiRoot + 'single/oms/detail', //订单详情
  OrderCancel: WxApiRoot + 'single/oms/closeOrder', //取消订单
  OrderRefund: WxApiRoot + 'single/oms/applyRefund', //退款取消订单
  OrderDelete: WxApiRoot + 'single/oms/closeOrder', //删除订单
  OrderConfirm: WxApiRoot + 'single/oms/confimDelivery', //确认收货
  OrderGoods: WxApiRoot + 'order/goods', // 代评价商品信息
  OrderComment: WxApiRoot + 'order/comment', // 评价订单商品信息

  AftersaleSubmit: WxApiRoot + 'single/oms/saveOmsOrderReturnApply', // 提交售后申请
  AftersaleList: WxApiRoot + 'single/oms/order/aftersaleslist', // 售后列表
  AftersaleDetail: WxApiRoot + 'single/oms/order/aftersalesinfo', // 售后详情

  FeedbackAdd: WxApiRoot + 'feedback/submit', //添加反馈
  FootprintAdd: WxApiRoot + 'single/pms/addView', //添加商品浏览记录
  FootprintList: WxApiRoot + 'single/pms/viewList', //足迹列表
  FootprintDelete: WxApiRoot + 'footprint/delete', //删除足迹

  GroupOnList: WxApiRoot + 'single/pms/groupHotGoods/list', //团购列表
  GroupOnMy: WxApiRoot + 'groupon/my', //团购API-我的团购
  GroupOnDetail: WxApiRoot + 'single/pms/goodsGroup/detail', //团购API-详情
  GroupOnJoin: WxApiRoot + 'groupon/join', //团购API-详情

  CouponList: WxApiRoot + 'single/home/selectNotRecive', //优惠券列表
  CouponMyList: WxApiRoot + 'single/sms/listMemberCoupon', //我的优惠券列表
  CouponSelectList: WxApiRoot + 'single/oms/couponSelectList', //当前订单可用优惠券列表
  CouponReceive: WxApiRoot + 'single/sms/add', //优惠券领取
  CouponExchange: WxApiRoot + 'coupon/exchange', //优惠券兑换

  StorageUpload: WxApiRoot + 'single/home/upload', //图片上传,

  UserIndex: WxApiRoot + 'single/oms/order.getorderstatusnum', //个人页面用户相关信息
  IssueList: WxApiRoot + 'issue/list', //帮助信息
};